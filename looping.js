var sayHello = "No. 1 Looping While" 
console.log(sayHello)
var sayHello = "LOOPING PERTAMA" 
console.log(sayHello)

var up = 1;
while(up < 22){
    if(up%2==0){
        console.log(up + ' - I Love Coding')
    }
    up++    
}

var sayHello = "LOOPING KEDUA" 
console.log(sayHello)

var down = 20;
while(down > 1){
    if(down%2==0){
        console.log(down + ' - I will become a mobile developer')
    }
    down--
}

var sayHello = "-------------------" 
console.log(sayHello)
var sayHello = "No. 2 Looping For" 
console.log(sayHello)

var num = 1

for (var i=1;i<=20;i++){
    if(i%2==0){
        console.log(i + ' - Berkualitas');
    }
    else if (i%3==0 && i%2==1){
        console.log(i + ' - I Love Coding');
    }
    else {console.log(i + ' - Santai');
    }
}
var sayHello = "-------------------" 
console.log(sayHello)
var sayHello = "No. 3 Membuat Persegi Panjang" 
console.log(sayHello)

var s = ''
for(var i = 0; i<4; i++){
    for(var j = 0; j < 8; j++){
        s += '#';
    }
    s += '\n'
}
console.log(s)

var sayHello = "-------------------" 
console.log(sayHello)
var sayHello = "No. 4 Membuat Tangga" 
console.log(sayHello)

var pagar = '#';
for(var angka4 = 1; angka4 <= 7; angka4++){
    console.log(pagar)
    pagar = pagar + '#'
}

var sayHello = "-------------------" 
console.log(sayHello)
var sayHello = "No. 5 Papan Catur" 
console.log(sayHello)

var catur = ''
for (var x = 0; x < 8; x++) {
    for (var y = 0; y < 8; y++) {
        if((y % 2 == 0 && x % 2 == 0) || (x % 2 == 1 && y % 2 == 1)){
            catur += ' '

        } else {
            catur += '#'
        }
    }
    catur += '\n'
}
console.log(catur);