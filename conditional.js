var sayHello = "SOAL 1 If-Else" 
console.log(sayHello)


var nama = "Bejo";
var peran = "Guard";

if(nama === "") {
    console.log("Nama harus diisi!");
} else { 
    console.log("Selamat datang di dunia warewolf," + nama)
    if(peran === "penyihir") {
        console.log(
            "Halo Penyihir " + 
            nama +
            ",kamu dapat melihat siapa yang menjadi werewolf."
        );
    } else if(peran === "Guard") {
        console.log(
            "Halo Guard " + 
            nama +
            ",kamu akan melindungi temanmu dari warewolf."
        );
    } else if(peran === "werewolf") {
        console.log(
            "Halo Werewolf " + nama + ",kamu akan memangsa setiap malam."
        );
    }
      
}

console.log()

var sayHello = "................................" 
console.log(sayHello)
var sayHello = "SOAL 2 Switch Case" 
console.log(sayHello)

var tanggal = 21
var bulan = 1
var tahun = 1945

switch (bulan) {
    case 1:
        bulan = "Januari";
        break;
    case 2:
        bulan = "Februari";
        break;
    case 3:
        bulan = "Maret";
        break;
    case 4:
        bulan = "April";
        break;
    case 5:
        bulan = "Mei";
        break;
    case 6:
        bulan = "Juni";
        break;
    case 7:
        bulan = "Juli";
        break;
    case 8:
        bulan = "Agustus";
        break;
    case 9:
        bulan = "September";
        break;
    case 10:
        bulan = "Oktober";
        break;
    case 11:
        bulan = "November";
        break;
    case 12:
        bulan = "Desember";
        break;      
}
console.log(tanggal + bulan + tahun);

