var sayHello = "SOAL 1!" 
console.log(sayHello)

var word = 'JavaScript '; 
var second = 'is '; 
var third = 'awesome '; 
var fourth = 'and '; 
var fifth = 'I '; 
var sixth = 'love '; 
var seventh = 'it!';
console.log(word.concat(second.concat(third.concat(fourth.concat((fifth.concat(sixth.concat(seventh))))))));

var sayHello = "............." 
console.log(sayHello)
var sayHello = "SOAL 2!" 
console.log(sayHello)

var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ;
var kata3 = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var kata4 = sentence[11] + sentence[12]; 
var kata5 = sentence[14] + sentence[15];
var kata6 = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var kata7 = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; 
var kata8 = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + kata3); 
console.log('Fourth Word: ' + kata4); 
console.log('Fifth Word: ' + kata5); 
console.log('Sixth Word: ' + kata6); 
console.log('Seventh Word: ' + kata7); 
console.log('Eighth Word: ' + kata8);


var sayHello = "............." 
console.log(sayHello)
var sayHello = "SOAL 3!" 
console.log(sayHello)

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substr(0, 3); 
var secondWord2 = sentence2.substr(4, 10);
var thirdWord2 = sentence2.substr(15, 2);
var fourthWord2 = sentence2.substr(18, 2); 
var fifthWord2 = sentence2.substr(21, 4); 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

var sayHello = "............." 
console.log(sayHello)
var sayHello = "SOAL 4!" 
console.log(sayHello)

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var panjang1 = exampleFirstWord3.length;
var panjang2 = secondWord3.length;
var panjang3 = thirdWord3.length;
var panjang4 = fourthWord3.length;
var panjang5 = fifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + panjang1); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + panjang2); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + panjang3);
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + panjang4);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + panjang5);